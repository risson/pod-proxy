FROM rust:1.58.0 as builder

WORKDIR /
RUN USER=root cargo new --bin pod-proxy
WORKDIR ./pod-proxy

RUN --mount=type=bind,target=./Cargo.toml,src=./Cargo.toml \
    --mount=type=bind,target=./Cargo.lock,src=./Cargo.lock \
    --mount=type=cache,target=/root/.cargo \
    cargo build --release && \
    rm src/*.rs target/release/deps/pod_proxy*

COPY ./ ./

RUN --mount=type=bind,target=./Cargo.toml,src=./Cargo.toml \
    --mount=type=bind,target=./Cargo.lock,src=./Cargo.lock \
    --mount=type=cache,target=/root/.cargo \
    cargo build --release

FROM debian:buster-slim

ARG WORKDIR=/usr/src/app

RUN apt-get update \
    && apt-get install -y ca-certificates tzdata \
    && rm -rf /var/lib/apt/lists/*

ENV TZ=Etc/UTC \
    APP_USER=appuser

RUN groupadd $APP_USER \
    && useradd -g $APP_USER $APP_USER \
    && mkdir -p ${WORKDIR}

COPY --from=builder /pod-proxy/target/release/pod-proxy ${WORKDIR}/pod-proxy

RUN chown -R $APP_USER:$APP_USER ${WORKDIR}

USER $APP_USER
WORKDIR ${WORKDIR}

CMD ["./pod-proxy"]
