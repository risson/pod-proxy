use k8s_openapi::api::core::v1::Pod;
use kube::{
    api::{Api, ListParams, ResourceExt},
    Client,
};
use rocket::http::Status;
use rocket::response::content::Html;
use rocket::response::status;
use rocket::State;
use std::collections::HashMap;

#[macro_use]
extern crate rocket;

fn is_pod_ready(p: &Pod) -> bool {
    let status = p.status.as_ref().unwrap();
    if let Some(conds) = &status.conditions {
        let failed = conds
            .iter()
            .filter(|c| c.type_ == "Ready" && c.status == "False")
            .map(|c| c.message.clone().unwrap_or_default())
            .collect::<Vec<_>>()
            .join(",");
        if !failed.is_empty() {
            return false; // ignore job based pods, they are meant to exit 0
        }
    }
    true
}

#[get("/")]
async fn index(api: &State<Api<Pod>>) -> status::Custom<Html<String>> {
    let label_filter =
        std::env::var("LABEL_FILTER").unwrap_or_else(|_| "".into());
    let pods =
        match api.list(&ListParams::default().labels(&label_filter)).await {
            Ok(pods) => pods,
            Err(_) => {
                return status::Custom(
                    Status::InternalServerError,
                    Html("Couldn't list pods".to_string()),
                )
            }
        };

    let target_url = std::env::var("TARGET_URL_TEMPLATE")
        .unwrap_or_else(|_| "http://localhost/proxy/{pod}".into());

    let mut response = String::from("<h1>Pod list</h1>");
    response += "<ul>";
    for pod in &pods {
        if !is_pod_ready(pod) {
            continue;
        }

        response += "<li><a href=\"";

        let mut vars = HashMap::new();
        let pod_name = pod.name().clone();
        vars.insert("pod".to_string(), pod_name.as_str());
        response +=
            strfmt::strfmt(target_url.as_str(), &vars).unwrap().as_str();

        response += "\">";
        response += pod.name().clone().as_str();
        response += "</a></li>";
    }
    response += "</ul>";

    status::Custom(Status::Ok, Html(response))
}

#[get("/list")]
async fn api_list(api: &State<Api<Pod>>) -> status::Custom<String> {
    let label_filter =
        std::env::var("LABEL_FILTER").unwrap_or_else(|_| "".into());
    let pods =
        match api.list(&ListParams::default().labels(&label_filter)).await {
            Ok(pods) => pods,
            Err(_) => {
                return status::Custom(
                    Status::InternalServerError,
                    "Couldn't list pods".to_string(),
                )
            }
        };
    let mut response = String::new();
    for pod in &pods {
        if !is_pod_ready(pod) {
            continue;
        }

        response += pod.name().clone().as_str();
        response += " ";
        response += pod.status.as_ref().unwrap().pod_ip.as_ref().unwrap();
        response += "\n";
    }

    status::Custom(Status::Ok, response)
}

#[get("/healthz")]
fn healthz() -> &'static str {
    "ok"
}

#[launch]
async fn rocket() -> _ {
    let client = Client::try_default().await.unwrap();
    let namespace =
        std::env::var("NAMESPACE").unwrap_or_else(|_| "default".into());
    let api = Api::<Pod>::namespaced(client, &namespace);

    rocket::build()
        .manage(api)
        .mount("/", routes![index, healthz])
        .mount("/api", routes![api_list])
}
